package utils

import "os/exec"
import "syscall"

// Run start command in minimized window and returns output
func Run(cmd string) string {
	e := exec.Command("cmd", "/C", cmd)
	e.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	e.Run()
	res, _ := e.Output()
	return string(res)
}

// Please ask for admin using UAC and runs command
func Please(RawCommand string) string {
	return Run("powershell.exe -Command Start-Process -Verb RunAs " + string(RawCommand))
}
